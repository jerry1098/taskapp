import 'package:uuid/uuid.dart';

class Task {
  final int type;
  final String uuid;
  final int createdAt;
  final String task;
  final int daily;
  final int done;

  Task({this.task, this.type, this.uuid, this.createdAt, this.daily, this.done});

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
      'type': type,
      'createdAt': createdAt,
      'task': task,
      'daily': daily,
      'done': done
    };
  }

}