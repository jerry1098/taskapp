import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:theo_todo/ui/screens/task_page.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart' as vector_math;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:theo_todo/bloc/task_overview_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<TaskOverviewBloc>(context).add(TaskOverviewEventGetInfo());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).accentColor,
        appBar: AppBar(
          backgroundColor: Theme.of(context).accentColor,
          title: Text('trytask', style: Theme.of(context).textTheme.headline1),
          centerTitle: true,
        ),
        body: TaskOverview());
  }
}

class TaskOverview extends StatelessWidget {
  const TaskOverview({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskOverviewBloc, TaskOverviewState>(
      builder: (context, state) {
        if (state is TaskOverviewInitialized) {
          return SizedBox.expand(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  height: 40,
                ),
                singleTaskOverviewCard(
                    'TODAY',
                    state.todayOverview,
                    state.todayMore,
                    state.todayMissed,
                    state.todayAmount,
                    context,
                    0),
                singleTaskOverviewCard('SOON', state.soonOverview,
                    state.soonMore, false, state.soonAmount, context, 1),
                singleTaskOverviewCard(
                    'LONG TERM',
                    state.longTermOverview,
                    state.longTermMore,
                    false,
                    state.longTermAmount,
                    context,
                    2),
              ],
            ),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget singleTaskOverviewCard(String title, List<String> tasks, bool more,
      bool missed, int amount, BuildContext context, int id) {
    return Expanded(
      child: Column(children: <Widget>[
        InkWell(
          onTap: () {
            Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => TaskPage(),
                ));
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              bookmarkWithNumber(amount, missed, context),
              Container(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 5,
                      ),
                      Container(
                        width: 318,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            tasks.length == 0
                                ? Padding(
                                    padding: EdgeInsets.all(15.0),
                                    child: Text(
                                      'Click here to add a new task',
                                      style: GoogleFonts.montserrat(
                                          color: Colors.black26),
                                    ))
                                : Container(),
                            tasks.length > 0
                                ? Text(
                                    tasks[0],
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                : Container(),
                            tasks.length > 1
                                ? Text(
                                    tasks[1],
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                : Container(),
                            tasks.length > 2
                                ? Text(
                                    tasks[2],
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                : Container(),
                            more
                                ? Text('more . . .',
                                    style:
                                        Theme.of(context).textTheme.bodyText2)
                                : Container(),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ]),
    );
  }

  Widget bookmarkWithNumber(int number, bool marked, BuildContext context) {
    return Column(children: <Widget>[
      Container(
        height: 30,
      ),
      Container(
        height: 70,
        width: 70,
        child: CustomPaint(
          painter: BookmarkPainter(
              color: Theme.of(context).primaryColor,
              number: number.toString(),
              missed: marked,
              textStyle: Theme.of(context).textTheme.headline3),
        ),
      ),
    ]);
  }
}

class BookmarkPainter extends CustomPainter {
  Color color;
  Color missedColor = Color.fromRGBO(230, 74, 25, 1);
  bool missed;
  String number;
  TextStyle textStyle;

  BookmarkPainter({this.color, this.number, this.missed, this.textStyle});

  @override
  void paint(Canvas canvas, Size size) {
    double yOffset = size.height / 14;

    if (number.length > 2) {
      number = '∞';
    }

    Paint mainPaint = Paint()
      ..color = color
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;
    Paint missedPaint = Paint()
      ..color = missedColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;

    Rect rect = Rect.fromPoints(
        Offset(0, yOffset), Offset(size.width / 2, size.height));

    canvas.drawCircle(size.center(Offset(0, yOffset / 2)),
        (size.height - yOffset) / 2, mainPaint);
    canvas.drawRect(rect, mainPaint);

    if (missed) {
      double degree = 50;
      double radians = vector_math.radians(degree);
      double radius = size.height / 2;
      double missedX = radius * cos(radians);
      double missedY = radius * sin(radians);
      missedX = size.center(Offset(0, yOffset)).dx + missedX;
      missedY = size.center(Offset(0, yOffset)).dy + missedY * -1;
      canvas.drawCircle(Offset(missedX, missedY), size.height / 8, missedPaint);
    }

    final textSpan = TextSpan(text: number, style: textStyle);

    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );

    textPainter.layout(minWidth: 0, maxWidth: size.width);
    textPainter.paint(
        canvas,
        size.centerLeft(Offset(size.width / 2.5 - (textPainter.width / 2),
            (textPainter.height / 2) * -1)));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
