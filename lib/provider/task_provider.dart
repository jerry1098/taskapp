import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:theo_todo/models/task.dart';

class TaskProvider {
  Database database;

  initDatabase() async {
    if (database != null) return;
    print(join(await getDatabasesPath(), 'task_database.db'));
    print('Hey');
    database = await openDatabase(
      join(await getDatabasesPath(), 'task_database.db'),
      onCreate: (db, version) {
        db.execute(
          'CREATE TABLE task_table(uuid STRING PRIMARY KEY, type INTEGER, createdAt INTEGER, task STRING, daily INTEGER, done INTEGER)',
        );
        db.execute(
            'CREATE VIEW IF NOT EXISTS daily_tasks as SELECT uuid, createdAt, task, daily, done FROM task_table WHERE type=0 ORDER BY done, daily, createdAt');
        db.execute(
            'CREATE VIEW IF NOT EXISTS soon_tasks as SELECT uuid, createdAt, task, done FROM task_table WHERE type=1 ORDER BY done, createdAt');
        db.execute(
            'CREATE VIEW IF NOT EXISTS long_term_tasks as SELECT uuid, createdAt, task, done FROM task_table WHERE type=2 ORDER BY done, createdAt');
      },
      version: 1,
    );
  }

  Future<void> insertTask(Task task) async {
    await initDatabase();
    print('Inserting new Task into db');
    await database.insert(
      'task_table',
      task.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> clearTable() async {
    await initDatabase();
    print('Clearing table');
    await database.execute('DELETE FROM task_table');
    await database.execute('VACUUM');
  }

  Future<void> printTable(String tableName) async {
    await initDatabase();
    (await database.query(tableName)).forEach((element) {
      print(element);
    });
  }

  Future<List<Task>> getOverviewForTable(String tableName) async {
    await initDatabase();
    final List<Map<String, dynamic>> maps =
        await database.query(tableName, limit: 4);
    return List.generate(maps.length, (index) {
      return _mapToTask(maps[index]);
    });
  }

  Future<int> getToDoAmountForTable(String tableName) async {
    await initDatabase();
    final List<Map<String, dynamic>> maps =
        await database.query(tableName, where: 'done=0');
    return maps.length;
  }

  Task _mapToTask(Map<String, dynamic> map) {
    return Task(
        uuid: map['uuid'],
        createdAt: map['createdAt'],
        daily: map['daily'],
        done: map['done'],
        task: map['task'],
        type: map['type']);
  }
}
