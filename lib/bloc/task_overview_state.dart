part of 'task_overview_bloc.dart';

abstract class TaskOverviewState extends Equatable {
  const TaskOverviewState();
}

class TaskOverviewInitial extends TaskOverviewState {
  @override
  List<Object> get props => [];
}

class TaskOverviewInitialized extends TaskOverviewState {
  final List<String> todayOverview;
  final bool todayMore;
  final bool todayMissed;
  final int todayAmount;
  final List<String> soonOverview;
  final bool soonMore;
  final int soonAmount;
  final List<String> longTermOverview;
  final bool longTermMore;
  final int longTermAmount;

  TaskOverviewInitialized(
      this.todayOverview,
      this.todayMore,
      this.todayMissed,
      this.todayAmount,
      this.soonOverview,
      this.soonMore,
      this.soonAmount,
      this.longTermOverview,
      this.longTermAmount,
      this.longTermMore);

  @override
  List<Object> get props => [
        todayOverview,
        todayMore,
        todayMissed,
        todayAmount,
        soonOverview,
        soonMore,
        soonAmount,
        longTermOverview,
        longTermMore,
        longTermAmount
      ];
}
