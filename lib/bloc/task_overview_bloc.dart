import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:mock_data/mock_data.dart';
import 'package:theo_todo/models/task.dart';
import 'package:theo_todo/provider/task_provider.dart';
import 'package:uuid/uuid.dart';

part 'task_overview_event.dart';
part 'task_overview_state.dart';

class TaskOverviewBloc extends Bloc<TaskOverviewEvent, TaskOverviewState> {
  final TaskProvider taskProvider;

  TaskOverviewBloc({@required this.taskProvider})
      : assert(taskProvider != null);

  @override
  TaskOverviewState get initialState => TaskOverviewInitial();

  @override
  Stream<TaskOverviewState> mapEventToState(
    TaskOverviewEvent event,
  ) async* {
    if (event is TaskOverviewEventGetInfo) {
      List<String> todayOverview = List();
      bool todayMore = false;
      bool todayMissed = false;
      int todayAmount;
      List<String> soonOverview = List();
      bool soonMore = false;
      int soonAmount;
      List<String> longTermOverview = List();
      bool longTermMore = false;
      int longTermAmount;

      (await taskProvider.getOverviewForTable('daily_tasks'))
          .forEach((element) {
        todayOverview.add(element.task);
        DateTime dateCreated =
            DateTime.fromMillisecondsSinceEpoch(element.createdAt);
        DateTime dateNow = DateTime.now();
        Duration diff = dateNow.difference(dateCreated);
        if (diff.inHours > 24) todayMissed = true;
      });
      (await taskProvider.getOverviewForTable('soon_tasks')).forEach((element) {
        soonOverview.add(element.task);
      });
      (await taskProvider.getOverviewForTable('long_term_tasks'))
          .forEach((element) {
        longTermOverview.add(element.task);
      });

      todayAmount = await taskProvider.getToDoAmountForTable('daily_tasks');
      soonAmount = await taskProvider.getToDoAmountForTable('soon_tasks');
      longTermAmount =
          await taskProvider.getToDoAmountForTable('long_term_tasks');

      if (todayOverview.length > 3) todayMore = true;
      if (soonOverview.length > 3) soonMore = true;
      if (longTermOverview.length > 3) longTermMore = true;

      yield TaskOverviewInitialized(
          todayOverview,
          todayMore,
          todayMissed,
          todayAmount,
          soonOverview,
          soonMore,
          soonAmount,
          longTermOverview,
          longTermAmount,
          longTermMore);
    }
    if (event is TaskOverviewEventTextingInsert) {
      Random random = Random();
      for (int i = 0; i < 400; i++) {
        print("Currently at " + i.toString());
        int type = random.nextInt(3);
        Task task = Task(
            createdAt: mockDate(DateTime(2020, 4, 1)).millisecondsSinceEpoch,
            type: type,
            daily: type != 0 ? 0 : random.nextInt(2),
            done: random.nextInt(2),
            task:
                'Really Really Really Really Really Very Very Very Long Long Tasks To test some stuff in my suiper duper app (just for testing tight now)',
            uuid: Uuid().v1());
        taskProvider.insertTask(task);
        await Future.delayed(Duration(milliseconds: 100));
      }
      print('Done inserting');
    }
  }
}
