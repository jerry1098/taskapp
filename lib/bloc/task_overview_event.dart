part of 'task_overview_bloc.dart';

abstract class TaskOverviewEvent extends Equatable {
  const TaskOverviewEvent();
  List<Object> get props => [];
}

class TaskOverviewEventGetInfo extends TaskOverviewEvent {}

class TaskOverviewEventTextingInsert extends TaskOverviewEvent {}
