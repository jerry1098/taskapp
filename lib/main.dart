import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:theo_todo/bloc_injector.dart';
import 'package:theo_todo/ui/screens/home_page.dart';

void main() => runApp(TaskApp());

class TaskApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'AlphaTask',
        home: BlocInjector(
          child: HomePage(),
        ),
        theme: ThemeData(
          primaryColor: Color.fromRGBO(48, 58, 82, 1),
          accentColor: Color.fromRGBO(245, 238, 237, 1),
          appBarTheme: AppBarTheme(elevation: 0),
          textTheme: TextTheme(
            headline1: GoogleFonts.montserrat(
                fontWeight: FontWeight.w800,
                fontSize: 40.0,
                color: Color.fromRGBO(48, 58, 82, 0.5),
                letterSpacing: 25),
            headline2: GoogleFonts.montserrat(
              fontWeight: FontWeight.w600,
              fontSize: 30.0,
              color: Color.fromRGBO(48, 58, 82, 1),
            ),
            bodyText1: GoogleFonts.montserrat(
                fontWeight: FontWeight.w400,
                fontSize: 20.0,
                color: Colors.black),
            bodyText2: GoogleFonts.montserrat(
                fontWeight: FontWeight.w400,
                fontSize: 20.0,
                color: Colors.black45),
            headline3: GoogleFonts.montserrat(
                fontWeight: FontWeight.w600,
                fontSize: 35.0,
                color: Color.fromRGBO(245, 238, 237, 1)),
          ),
        ));
  }
}
