import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:theo_todo/bloc/task_overview_bloc.dart';
import 'package:theo_todo/provider/task_provider.dart';

class BlocInjector extends StatelessWidget {
  const BlocInjector({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    TaskProvider taskProvider = TaskProvider();
    return BlocProvider<TaskOverviewBloc>(
      create: (context) => TaskOverviewBloc(taskProvider: taskProvider),
      child: child,
    );
  }
}
